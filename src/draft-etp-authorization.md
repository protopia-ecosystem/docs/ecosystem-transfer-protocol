## Авторизация.

### Процедуру получения токена

Для доступа к ресурсам (авторизации) клиент должен произвести процедуру получения токена доступа и аутентификации.

Так как GraphQL является endpoint-протоколом передачи данных и не позволяет использовать “redirect flow” (пользователю не доступна строка браузера), то авторизация, идентификация и аутенфикация клиента (сервиса) и конечного пользователя происходит через разъединенный поток (“decoupled flow”).

На общем уровне, весь процесс выглядит следующим образом:

1. Сервис авторизуется в системе с помощью самоподписанного токена. 
2. Пользователь идентифицируется идентификационным сервисом (OP) и получает токен пользователя (user_token) в виде идентификационного токена пользователя (id token), токена входа (login token) или персонального токена пользователя (passport). Этот токен в формате JWT содержит набор определенных полей (claims) с информацией о пользователе.
3. Пользователь использует доверенный канал связи с сервером (OOB) и запрашивает одноразовый пароль для входа (OTP). В качестве доверенных каналов точно могут быть использована почта пользователя и телефон. При подтверждении пользователя у провайдера идентификационного сервиса (OP) в качестве доверенного канала могут быть использован канал идентификационного сервиса.
4. Пользователь, как владелец ресурса (RO) дает разрешение приложению (Client) на доступ к определенному ресурсу в виде гранта (права). По подтвержденному каналу связи пользователю приходит расшифровка этого гранта. См UMA 2.0
5. Приложение обращается к серверу авторизации (AS) и получает токен доступа к ресурсу (“access_token”) в обмен на свой грант и предоставленный “user_token”.
Приложение использует этот токен (“access_token”)  для получения требуемых данных от сервера ресурсов (RS).

Существует несколько типов приложений, которые могут авторизовываться через “decoupled flow” это:

- Back-Channel Client -
- Single Page Application (SPA) - 
- Native Application  -

В системе существует 7 видов грантов получения ключей доступа:


1. JWT-Bearer - “jwt-bearer” Обмен самоподписанного токена на авторизационный.
2. JWT-OTP - .
3. CIBA POLL - “backchannel_request” (“ciba”) скрытый канал авторизации Грант применяется для получения токена по сессии аутенфикации “auth_req_id”. В режиме Poll клиент получает результат аутентификации путем опроса конечной точки токена OP с использованием нового типа предоставления. “backchannel_request” auth_req_id [CIBA core]
4. CIBA PING - “ciba” Грант применяется для получения токена по сессии аутенфикации “auth_req_id”. В режиме Ping OP опубликует уникальный идентификатор сеанса аутентификации на клиенте, затем клиент получит результат аутентификации из конечной точки токена, используя новый тип предоставления. [CIBA core]
5. CIBA PUSH - “ciba” Грант применяется для получения токена по сессии аутенфикации “auth_req_id”. В режиме Push, OP опубликует полный результат аутентификации для Клиента. [CIBA core]
6. Device Code - “device_code” получение токена для конкретного устройства.
7. Token Delegation - “token-exchange” делегирование токена.
8. Refresh Token - по существующему токену

Для использования методов “decoupled flow” Клиент ДОЛЖЕН быть аутентифицирован и иметь токен доступа выданный на его имя.
В качестве “access_tokena” используется JWT с дополнительными полями указывающими на какому клиенту и для каких целей выдан данный токен. 

В случае “ping” и “push” методе клиент получает callback сообщение, подписанное  “client_notification_token”. Этот токен используются сервисом для аутентификации запроса обратного вызова. Он предоставляется клиентом и совпадает с “client_token”. 

Для нотификации может быть использован graphql-механизм subscriptions или callback-функций.

### Ассоциация пользователей. Одноразовый пароль.

Чтобы избежать входа в систему злоумышленником, для достоверного подтверждения идентификации и авторизации пользователя будет использоваться двухфакторная (многофакторная) схема авторизации. 

В качестве каналов получения пароля могут быть использованы достоверные каналы, то есть каналы прошедшие верификацию.Свойства пользователя verifed должны иметь значение true. Ниже приведены свойства пользователя отвечающие за это:

email_verifed - подтвержденный email
phone_verifed - подтвержденный телефон
external_verifed - иная подтвержденная система

Запрос на получение одноразового пароля происходит по graphql команде: “associate”:

(MFA_token)

Mutation{
associate(input: AuthenticatorInput): AuthenticatorResponce
}

input AuthenticatorInput{
   authenticator_types: String
   oob_channels: String
login_hint: String
}

authenticator_types: 
"otp" (one-time password, одноразовый пароль) - генерация одноразового пароля
"oob" - Безопасное взаимодействие с сервером авторизации по вторичному каналу.
"recovery-code" - список кодов (?) //recovery_codes 
oob_channels - каналы для получения кода. Должны совпадать с параметром “amr” в логин токене (“login_token”).
"sms" - смс, login_hint = phone_number
"tel" - телефон, login_hint = phone_number
"email" - высылается по почте, login_hint = email
“associate_hint” - подсказка ассоциации. Заменяет “phone_number”, “email” из [OAuth 2.0 Multi-Factor Authenticator Association] 

type AuthenticatorResponce{

   authenticator_type: String
   oob_channel: String
   user_code: String

}

authenticator_type - Тип аутентификатора, связанный с учетной записью пользователя.
oob_channel - Вторичный канал для использования. Параметр ДОЛЖЕН быть включен, когда значение «authenticator_type» равно «oob», и НЕ ДОЛЖЕН включаться в других случаях.
user_code - одноразовый пароль или код подтверждения. Чтобы обеспечить большую совместимость с протоколом CIBA параметр назван “user_code”. По назначению параметр совпадает с secret или oob_code нотификации OAuth 2.0 Multi-Factor Authorization. 
Авторизация устройства

### Авторизация пользователя.

Authorization (AuthZ) is the process where a Trustor Delegates a Permission to a Trustee to perform a privilege against a Target Resource
Allowing an Entity to do something. (Thing Explainer)

Запрос прав доступа происходит по graphql команде: “authorize”:

Mutation{
authorize(intput: AuthorizeInput): AuthorizeResponse
}

На этапе авторизации пользователь неявно логиниться.

Пользователь имеет возможность предоставить доступ к своим ресурсам посредством авторизации клиента.

Доступ клиента к данным пользователя или его ресурсам ограничен "областью видимости" (scope).

input AuthorizeInput{

   scope: [String]!
   state: String

   login_hint: String
   login_hint_token: String
   id_token_hint: String
   
   device_code: String
   user_code: String

   client_notification_token: String

}

scope - права
state - набор чисел и букв
Подсказки идентификация пользователя login hints:
id_token_hint - подсказка токена идентификации. (Для OP)
login_hint_token - токен идентификации пользователя в протоколе CIBA. (Для SPA)
login_hint - подсказка поля идентификации.
user_code - Секретный код, такой как пароль(password) или пин-код(pin), известный только пользователю, но проверяемый OP. Код используется для авторизации отправки запроса аутентификации на устройство аутентификации пользователя.
device_code - 
client_notification_token - опционально. токен нотификации.


type AuthorizeResponse{

   auth_req_id: String

   state: String
   expires_in: String
   interval: String

}

auth_req_id - идентификатор авторизации.
expires_in
interval
state - набор чисел и букв из запроса

Запрос на получение токена. 

Access Control (or Privilege Management) is a process where an Authoritative Entity (Trustor) who grants a permission to a Trustee
Access Control is typically implemented within an Access Control Service
 

В двух-ступенчатой авторизации запрос на получение токена доступа происходит по graphql команде: “token”:

Mutation{
token(input: TokenInput): TokenResponse
}

input TokenInput{
   grant_type: String!
   scope: [String]
   auth_req_id: String
   device_code: String
   refresh_token: String
}

grant_type - тип “backchannel_request”, “device_code”, “token-exchange”, “jwt-bearer”
scope - область (права) scope=”openid” id_token - дает на выходе.
auth_req_id    
refresh_token - токен повторной авторизации
device_code - 


type TokenResponse{
   access_token: String!
   token_type: String!
   expires_in: String
   refresh_token: String
   id_token: String
}

access_token - токен доступа в формате JWT
token_type - тип токена: “Bearer”. (часть протокола)
expires_in - истекает (3600)
refresh_token - токен повторной авторизации в формате JWT
id_token - идентификационный токен в формате JWT, при “scope = openid”
Проверка токена
Проверку подлинности и содержания токена доступа можно сделать командой “introspect”. "OAuth 2.0 Token Introspection" стандартизирует взаимодействие проверки RS и AS и увеличит шансы на совместимость RS с AS. Команда  “introspect” отдает токен в структурированном виде.

Аналогично команде “introspect” выглядит “access_token” при дешифровке. Это сделано для того, чтобы RS мог проверить его самостоятельно. 

Query{
introspect(token: String, token_type_hint: String): Token
}

token - Токен в сериализованном виде (JWS, JWE)
token_type_hint - подсказка типа токена, может иметь значения: 
“access_token”
“id_token”

type Token {
     active: Boolean

     sub: String
     aud: String
     iss: String
     exp: Int (DataTime)
     iat: Int (DataTime)
     client_id: String
     scp: String
     }

active - токен может быть использован для доступа к ресурсам. true - валидный
Аутентификация.

Authentication for most of our purposes is the process a Digital Identity (Peggy) making an Assertion of Claims to a Verifier (Victor) which uses Authentication Methods to provide a Level Of Assurance by validation of the Claims. 

Аутенфикация проводиться по типу “Bearer” (барьерная) - по ключу доступа (“access_token”) или по “client_assertion”(?). Для этого высылается заголовок HTTP Header следующего вида “Authorization: Bearer <token>”

В качестве Bearer токена могут быть использованы:

Самоподписанные client_assertion токены для первичной аутентификации клиента. - ?
Токены доступа acess_token выданные авторизационным сервером.
Запросы к конечной точке могут быть аутентифицированы с использованием «Barier token», созданного клиентом и отправленного OP в запросе аутентификации в качестве значения параметра «client_notification_token». - ?

При аутентификации с помощью токена сервер ресурсов (RS) должно выполнить следующие проверки:
Токен был выдан доверенным идентификационным сервисом (AS): URI issuer должно совпадать с URI идентификационного сервиса.
Токен предназначается текущему сервису (RP): URI должно содержаться в списке в поле audience.
Срок действия токена еще не истек (значение даты и времени в поле expiration date больше текущей даты и времени).
Токен подлинный и не был изменен (подпись токена совпадает с публичным ключом авторизационного сервиса).
Logout

Пользователь может выйти из системы отозвав свой токен доступа, для этого ему необходимо вызвать команду “revoke”.

revoke(token: String token_type_hint:String): Boolean

token - токен
token_type_hint - подсказка типа токена: “refresh_token”, “access_token”

### ошибки
